package br.com.bluesoft.desafiov3.desafiov3.pedido.web.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import br.com.bluesoft.desafiov3.desafiov3.pedido.business.PedidoService;
import br.com.bluesoft.desafiov3.desafiov3.pedido.model.FormaPagamento;
import br.com.bluesoft.desafiov3.desafiov3.pedido.model.Pedido;
import br.com.bluesoft.desafiov3.desafiov3.pedido.model.exception.EstoqueVazioException;
import br.com.bluesoft.desafiov3.desafiov3.pedido.web.form.PedidoFormulario;

@RestController
@RequestMapping("/pedidos")
public class PedidoController {

    private PedidoService pedidoService;

    public PedidoController(PedidoService pedidoService) {
        this.pedidoService = pedidoService;
    }

    @PostMapping
    public Pedido novoPedido(@RequestBody PedidoFormulario pedidoFormulario) throws EstoqueVazioException {
        return pedidoService.novoPedido(pedidoFormulario);
    }

    @GetMapping
    public List<Pedido> listarPedidos() {
        return pedidoService.listarTodos();
    }

    @GetMapping(value = "/{pedidoId}")
    public FormaPagamento buscarPagamentoDeUmPedido(@PathVariable Long pedidoId) {
        return pedidoService.buscarPedido(pedidoId);
    }

    @DeleteMapping(value = "/{pedidoId}")
    public void deletarPedido(@PathVariable Long pedidoId) {
        pedidoService.deletarPedido(pedidoId);
    }

}
